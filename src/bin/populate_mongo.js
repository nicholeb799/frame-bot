const { connectClient } = require("../lib/mongo");

module.exports.updateMongoDB = async (dbName, frames) => {
  const client = await connectClient();
  const appDb = client.db(dbName);
  const collectionName = "frames";
  const collection = appDb.collection(collectionName);

  try {
    await collection.drop();
  } catch (e) {
    if (e.codeName !== "NamespaceNotFound") {
      console.log(e.ok, e.code, e.codeName);
      await client.close();
      throw e;
    }
  }

  console.time(dbName);
  console.log(`inserting ${frames.length} frames into db ${dbName} ...`);
  try {
    await collection.insertMany(frames);
  } catch (e) {
    console.log("caught error while inserting frames");
    console.log(e.ok, e.code, e.codeName);
    await client.close();
    throw e;
  }
  console.timeEnd(dbName);

  await collection.createIndex({ rnd: 1 });
  await collection.createIndex({ name: 1 });

  await client.close();
  return;
};
