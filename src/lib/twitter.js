require("dotenv").config();
const { readFile } = require("fs").promises;
const Twitter = require("twitter-lite");
const { parse } = require("path");
function newClient(subdomain = "api") {
  return new Twitter({
    subdomain,
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token_key: process.env.ACCESS_TOKEN_KEY,
    access_token_secret: process.env.ACCESS_TOKEN_SECRET,
  });
}
exports.postTweet = async (fullFilePath) => {
  const alt_text = parse(fullFilePath).name;
  const data = await readFile(fullFilePath);

  const base64Image = Buffer.from(data).toString("base64");
  const uploadClient = newClient("upload");
  // upload media
  let media;
  try {
    media = await uploadClient.post("media/upload", {
      media_data: base64Image,
    });
  } catch (e) {
    console.error(e);
    throw e;
  }
  // add metadata
  let metadataResponse;
  try {
    metadataResponse = await uploadClient.post("media/metadata/create", {
      media_id: media.media_id_string,
      alt_text: {
        text: alt_text,
      },
    });
  } catch (e) {
    console.error(e);
    throw e;
  }
  const apiClient = newClient();
  // post tweet with media and metadata
  let response;
  try {
    response = await apiClient.post("statuses/update", {
      media_ids: media.media_id_string,
    });
  } catch (e) {
    console.error(e);
    throw e;
  }
  return response;
};
