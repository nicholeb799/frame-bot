const { MongoClient } = require("mongodb");

exports.connectClient = async () => {
  const url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}?retryWrites=true&w=majority`;
  const client = new MongoClient(url);
  await client.connect();
  return client;
};

exports.getLatestTweets = async (tweets, memory) => {
  return tweets
    .aggregate([
      {
        $lookup: {
          from: "frames",
          localField: "frame_id",
          foreignField: "_id",
          as: "frame",
        },
      },
      {
        $sort: {
          tweetedAt: -1,
        },
      },
    ])
    .limit(memory)
    .toArray();
};
exports.getRandomDocument = async (frames) => {
  let choice = await frames
    .find({
      rnd: {
        $gte: Math.random(),
      },
    })
    .sort({ rnd: 1 })
    .limit(1)
    .toArray();

  if (choice.length < 1) {
    choice = await frames
      .find({ rnd: { $lte: Math.random() } })
      .sort({ rnd: 1 })
      .limit(1)
      .toArray();
  }
  if (choice.length < 1) {
    throw new Error("giving up!!!");
  }
  return choice[0];
};
