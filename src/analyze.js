require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const { analyzeVideos } = require("./bin/analyze_videos");
const options = [
  groups.pathOptions,
  args.folder,
  args.results,
  groups.emptyOptions,
  args.help,
];
(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("analyze.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run analyze-videos -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }
  console.log(await analyzeVideos(opts.folder, opts.results));
})();
