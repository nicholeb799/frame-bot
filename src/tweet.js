require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const options = [
  groups.pathOptions,
  args.folder,
  args.workspace,
  groups.cloudOptions,
  args.source,
  args.bucket,
  groups.emptyOptions,
  args.help,
];
(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("tweet.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log("usage: npm run tweet --  [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (!opts.folder) {
    console.error("unable to detect FOLDER in .env or argument '-f FOLDER'");
    return;
  }

  switch (opts.source) {
    case "DBX":
      const { tweetFromDropbox } = require("./bin/tweet");
      await tweetFromDropbox(opts.folder, opts.workspace);
      break;

    case "S3":
      if (!opts.bucket) {
        console.error("Unable to BUCKET in .env or argument '-b BUCKET'");
      } else {
        const { tweetFromS3 } = require("./bin/tweet");
        await tweetFromS3(opts.folder, opts.bucket, opts.workspace);
      }
      break;

    default:
      if (opts.source) {
        console.error(`Unrecognized source given "${opts.source}"`);
      } else {
        console.error("Unable to detect a SOURCE '-s SOURCE'");
      }
      break;
  }
})();
