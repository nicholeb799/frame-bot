const chai = require("chai");
const chaiHttp = require("chai-http");

const { describe } = require("mocha");
const { equal } = require("chai").assert;

chai.use(chaiHttp);
const app = require("../src/index");

describe("Redirect Server", function () {
  describe("#redirect()", function () {
    it("should redirect you when requesting the homepage", function (done) {
      chai
        .request(app)
        .get("/")
        .end(function (err, res) {
          equal(err, null, "error should equal null");
          equal(res.status, 200, "response should have status 200");
          equal(
            res.redirects[0],
            process.env.REDIRECT_URL,
            "response should redirect you to the URL set in .env"
          );
          done();
        });
    });
  });
});
