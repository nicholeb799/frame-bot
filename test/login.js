const chai = require("chai");
const chaiHttp = require("chai-http");

const { describe } = require("mocha");
const { equal } = require("chai").assert;

chai.use(chaiHttp);
var app = require("../src/login");

describe("OAuth Server", function () {
  describe("#index()", function () {
    it("should tell you to go to the /login/twitter page", function (done) {
      chai
        .request(app)
        .get("/")
        .end(function (err, res) {
          equal(err, null, "error should equal null");
          equal(res.status, 200, "response should have status 200");
          const message = res.body.message;
          equal(
            message.startsWith("go to "),
            true,
            'expected the message to start with "go to"'
          );
          equal(
            message.endsWith("open the OAuth login page"),
            true,
            'expected the message to end with "login page"'
          );
          done();
        });
    });
  });
});
