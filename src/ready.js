const { testFileReady } = require("./bin/check_if_ready");
(async function (testFile) {
  console.log(await testFileReady(testFile));
})(process.argv[2]);
