const {
  connectClient,
  getRandomDocument,
  getLatestTweets,
} = require("../lib/mongo");

function trimName(name, trim) {
  return name.slice(trim);
}

function splitFrameName(name, depth) {
  let split = name.split("/");
  // if depth is deeper than the length, return the last one
  if (depth >= split.length) {
    return split[split.length - 1];
  } else {
    return split[depth];
  }
}
function getLatestFrameNames(latestFrames, depth, trim) {
  return latestFrames.map((frame) => {
    return trimName(splitFrameName(frame.name, depth), trim);
  });
}
function getLatestFrames(latestTweets) {
  return latestTweets.map((x) => {
    return { name: x.frame[0].name, date: x.tweetedAt };
  });
}
async function getRandomFrame(
  frames,
  tweets,
  threshold, // tweets/frames ratio to reset tweets
  memory, // number of frames to remember before resetting and recent frames
  depth, // name of frame is split by '/' and select index with depth
  trim,
  debug
) {
  const frameCount = await frames.count({});
  const tweetCount = await tweets.count({});
  const latestTweets = await getLatestTweets(tweets, memory);
  const latestFrames = getLatestFrames(latestTweets);
  const latestFrameNames = getLatestFrameNames(latestFrames, depth, trim);
  if ((tweetCount / frameCount).toFixed(2) >= threshold) {
    await tweets.drop();
    await tweets.insertMany(latestTweets);
    if (debug)
      console.log('\n\nThreshold met! Dropped "tweets" collection!\n\n');
  }
  let frame;
  let newFrame = false;
  let tries = 0;
  while (!newFrame) {
    frame = await getRandomDocument(frames, tweets);
    const tweetedFrames = await tweets.countDocuments({ frame: frame._id });
    // check whether exact frame has been selected before
    if (tweetedFrames == 0) {
      const name = trimName(splitFrameName(frame.name, depth), trim);
      const index = latestFrameNames.indexOf(name);
      if (debug && tries > 0) console.log(`   ${index}  > ${name}`);
      // check whether a similar name has been used before
      if (index === -1) {
        newFrame = true;
      }
    }
    tries++;
  }

  const tweet = {
    tweetedAt: new Date(),
    frame_id: frame._id,
    tries: tries,
  };

  await tweets.insertOne(tweet);
  return frame;
}
function getFrameAndTweetCollections(client, dbName) {
  const db = client.db(dbName);
  const frames = db.collection("frames");
  const tweets = db.collection("tweets");
  return { frames, tweets };
}
module.exports.getFrameAndTweetCounts = async (dbName) => {
  const client = await connectClient();
  const { frames, tweets } = getFrameAndTweetCollections(client, dbName);
  const framesCount = await frames.count({});
  const tweetsCount = await tweets.count({});
  await client.close();
  return { framesCount, tweetsCount };
};
module.exports.chooseFrame = async (
  dbName,
  threshold,
  memory,
  depth,
  trimName,
  debug,
  client = null
) => {
  let noClient = client === null;
  if (noClient) {
    client = await connectClient();
  }
  const { frames, tweets } = getFrameAndTweetCollections(client, dbName);
  const frame = await getRandomFrame(
    frames,
    tweets,
    threshold,
    memory,
    depth,
    trimName,
    debug
  );
  if (noClient) {
    await client.close();
  }
  return frame;
};
module.exports.getClient = connectClient;
