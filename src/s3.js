require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const options = [
  groups.pathOptions,
  args.folder,
  args.workspace,
  groups.cloudOptions,
  args.bucket,
  args.endpoint,
  args.region,
  args.awsAccessKeyId,
  args.awsSecretAccessKey,
  groups.emptyOptions,
  args.help,
];

(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("s3.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run analyze-s3 -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }

  await require("./bin/s3").analyzeFolder(opts.bucket, opts.folder);
})();
