require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const options = [
  groups.pathOptions,
  args.folder,
  groups.cloudOptions,
  args.awsAccessKeyId,
  args.awsSecretAccessKey,
  args.endpoint,
  args.region,
  args.bucket,
  groups.dbOptions,
  args.host,
  args.name,
  args.user,
  args.pass,
  groups.emptyOptions,
  args.help,
];
const { updateMongoDB } = require("./bin/populate_mongo");
const { getObjects } = require("./lib/s3");
(async () => {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("mongo.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run populate-mongo --  [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }

  if (!opts.name) {
    console.error(
      "unagle to detect DB_NAME in .env or argument '--name DB_NAME'"
    );
    return;
  }
  if (!opts.folder) {
    console.error("unable to detect FOLDER in .env or argument '-f FOLDER'");
    return;
  }

  if (!opts.bucket) {
    console.error("Unable to detect BUCKET in .env or argument '-b BUCKET'");
    return;
  } else {
    console.log(`getting frames from ${opts.folder} ...`);
    const objects = await getObjects(opts.bucket, opts.folder);

    const frames = objects
      .map((obj) => {
        return {
          name: obj.Key,
          size: obj.Size,
          eTag: obj.ETag,
          rnd: Math.random(),
        };
      })
      .filter((frame) => frame.size > 0);
    await updateMongoDB(opts.name, frames);
    console.log("finished");
  }
})();
