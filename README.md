# frame-bot

This repo serves two purposes: Creating images (otherwise known as frames) from video files and posting the images to Twitter. Creating a series of images from a video is a common task: Here's a great page from the FFmpeg wiki that shows you some examples on how to use it directly:
https://trac.ffmpeg.org/wiki/Create%20a%20thumbnail%20image%20every%20X%20seconds%20of%20the%20video

(That's right, you don't even have to use this repo, it's all a sham)

This repository relies on the `select` filter to render the images. Checkout the API for more details:

https://ffmpeg.org/ffmpeg-filters.html#select_002c-aselect

# Table of Contents

1. [Getting Started](#getting-started)
1. [Installation](#installation)
1. [Analyzing Videos](#analyzing-videos)
1. [Making Frames](#making-frames)
1. [Frame Options](#frame-making-options)
1. [Processing Frames](#processing-frames)
1. [Posting Online](#posting-to-twitter)
1. [Choosing Frames](#choosing-frames)
1. [Testing](#testing)

---

# Getting Started

- required:
  - FFmpeg
    - https://ffmpeg.org/download.html
  - Node.js 14.x
    - https://nodejs.org/en/download/
- optional:
  - Hosting with API [DO Spaces, AWS S3, Dropbox]
    - https://docs.digitalocean.com/products/spaces/resources/
    - https://www.dropbox.com/developers
  - Twitter App
    - https://developer.twitter.com
  - Scheduled tasks [Heroku, DO Droplet, AWS ECS]
    - https://devcenter.heroku.com/articles/scheduler
    - https://www.digitalocean.com/community/tutorials/how-to-use-cron-to-automate-tasks-ubuntu-1804

## Installation

```
$ git clone git@gitlab...
$ cd frame-bot
$ npm install
$ cp .env.example .env
```

### Quick Test

Use the `ready` script to check if FFmpeg is installed correctly. Find a video file on your machine and use it with the `ready` script. For the following example, replace `../Videos/myTestVideo.mp4` with the _actual path_ of the video you want to test with:

```
$ npm run ready ../Videos/myTestVideo.mp4
video stream data: {"width":...}. Looks good, you should be able to make frames :)

```

If you see a message that displays meta-data (width, height, duration) and the `Looks good, ...` text, then you're ready 🍝

If you're confident that you have FFmpeg installed and this _still_ doesn't work, then post an issue with more details.

### Analyzing Videos

Before you make any frame, it will help to look at all of the files and their metadata at a glance. There is a script called `analyze-videos` and you give it a folder to analyze like so ...

```
$ npm run analyze-videos -- -f /mnt/c/Videos/Net\ Cafe\ \(WebRip\)
```

A listing will display all of the properties of the files in the folder (`/mnt/c/Videos/Net Cafe (WebRip)` from the example above).

This creates a `results.csv` file and saves it the root folder. This document will show all of the data at once so you can determine which dimensions should be applied to all of the frames (scaling & cropping). This script is also helpful in determing if all of the videos can be processed.

Here's a helpful graph of standard video dimensions:
https://upload.wikimedia.org/wikipedia/commons/0/0c/Vector_Video_Standards8.svg

Keep track of the dimensions for the files and use this graph to see which standard _most_ (hopefully all) of the videos are sized as. You'll need to use these measurements later if you want all of the frames to be the shaped identically.

### frame-math

The example below shows how to render an image every 1000 frames. Yes, **frames**, not seconds. Videos have framerates that determine how many frames are shown per second. For the sake of simplicity, we will assume most videos have a framerate of 30 frames per second (fps). Let's say we have a 6 minute video that is 30fps. We can estimate how many frames are in the video through multiplication:

```
30 fps * 6 minutes = (30 fps * 60 seconds) * 6 minutes = 10800 total frames
```

So if we set the _interval_ to 1000 frames, then we can expect about 10-11 frames from a 6 minute video. The 1000th frame, 2000th frame, and so on.

## Making Frames

So now that you know what to expect from the videos and how many images we'll be generating, you can start making the images by using the `make-frames` script:

```
$ npm run make-frames -- -f ../Videos/ -o ../output -n 1000
```

This will search through the `../Videos/` folder and for every video file it finds, creates a folder inside of `../output` of the same name. For example, if there is a video `../Videos/thanksgiving_parade_1989.mp4`, then a folder will be created as `../output/thanksgiving_parade_1989`. All of the images rendered from that video will be saved to this folder.

### Be careful

Think about how much space you have available on your disk! It's funny, but you can make a folder of images that's larger than the source file. It makes sense too! Imagine if you had a photograph of every frame of a movie. It would certainly occupy more space than the tape 📼

My advice is to play it safe, start with a large interval, make small adjustments, and be prepared to look at your input file properties (right-clicking the file).

#### Frame Making Options

The required arguments for the `make-frames` script are the input, output, and interval. Several other options are available for trimming, scaling, and cropping (even subtitles). These options are for available for reference through the help command:

```
$ npm run make-frames -- --help
```

#### File Format

To save the frames as a specific file type, use the `--format` option:

```
$ npm run make-frames -- -f /input -o /output --format jpg
```

##### Interval

This defaults to the `FRAME_INTERVAL` setting in the `.env` file, but can be used as `-n n` where n is the interval of frames to render. So if set to 100, then you get the 100th, 200th, 300th, and so on:

```
$ npm run make-frames -- -f /input -o /output -n 100
```

##### Subtitles

If the `--subtitles` option is present, then it will try to find a `.srt` or `.vtt` file that has the same-ish file name. Since these can vary, it will try its best to find the matching file.

```
$ npm run make-frames -- -i /input -o /output --subtitles
```

##### Trimming

There are regions to trim from a video, the start and the end. There's two arguments `--trimStart` and `--trimEnd`. They take one number that represents how many seconds to trim. For example, if you wanted to trim 10 seconds from the beginning:

```
$ npm run make-frames -- -f /input -o/output --trimStart 10
```

and if you want to remove the last 10 seconds:

```
$ npm run make-frames -- -f /input -o /output --trimEnd 10
```

##### Scaling

The best way to make all of the frames the same dimensions is to use a combination of arguments `--scale` and `--width` (and or) `--height`. For example, if you want to make all of the images scaled to VGA (640x480):

```
$ npm run make-frames -- -f /input -o /output --scale -w 640 -h 480
```

##### Cropping

There are four options for cropping an image:

- `-x` is the starting point on the x-axis
- `-y` is the starting point on the y-axis
- `-w` is the width of the output
- `-h` is the height of the output

For example, if you want to make images that are 1920 pixels wide, 1080 high, and crop the left side by 220 pixels.

```
$ npm run make-frames -- -i /input -o /output -x 220 -w 1920 -h 1080
```

These are all optional, and if you want to learn more, here's the cropping documentation:
https://ffmpeg.org/ffmpeg-filters.html#toc-crop

---

# Processing Frames

Now that you have a bunch of frames, there may be some worth deleting. Depending on how many there are, it's generally pretty easy to sort a folder by size and delete the smallest ones. It's a different story if you have many folders within folders. For that case, there is a script to identify images with low levels of brightness (luminance) and delete them (or hold for review).

## Luminance levels

Each image is comprised of pixels, and every pixel has value determining how bright it is. This script calculates those brightness levels (numbers between 0 and 1), and looks at how many there are in comparison (overall percentage). For example, here is an "image" containing luminance levels 0, 0.1, 0.5 and 0.9:

```
luminance of pixel [ ]
[ ] = 0
[░] = 0.01
[▒] = 0.05
[▓] = 0.09
╔══════╤══════╦══════╤══════╗
║      │      ║      │      ║
║      │      ║      │      ║
║──────┼──────╫──────┼──────╢
║      │░░░░░░║▒▒▒▒▒▒│▒▒▒▒▒▒║
║      │░░░░░░║▒▒▒▒▒▒│▒▒▒▒▒▒║
║──────┼──────╫──────┼──────╢
║      │░░░░░░║░░░░░░│▓▓▓▓▓▓║
║      │░░░░░░║░░░░░░│▓▓▓▓▓▓║
║──────┼──────╫──────┼──────╢
║      │░░░░░░║░░░░░░│░░░░░░║
║      │░░░░░░║░░░░░░│░░░░░░║
╚══════╧══════╩══════╧══════╝
```

We can map the levels and their percentages in format like:

```
{
        0.00=43%,       0.01=38%,        0.09=13%,        ...
}
```

This shows that the luminance level 0 is taking up 43% of the image, 0.01 has 38% of it, 0.09 has 13%, and there's more key(s) with 13% or less.

To consider the earlier image dark enough for removal, a "tolerance" for brightness would have to be 0.09 or higher. The most dominant brightness level (always the first item in the map), also has it's percentage measured against another tolerance, but called a ... "percentage". With the earlier example, a "percentage" of 0.43 or higher would delete the file. The command would look like:

```
$ npm run clean -- -f ./some-folder -t 0.09 -p 0.43
```

### Cleaning Options

There are two groups of options for cleaning, one is related to finding images, and the other is for determing whether to delete an image.

#### Cleaning (path) Options

##### Folder

Just like making the frames, pass a location containing images, or a folder containing folders that have images `-f ./some-folder`

##### Ignore

If you want to exclude any paths that contain a certain string to "ignore" then use `--ignore`

```
$ npm run clean -- -f ./some-folder --ignore extras
```

##### Trash

So this script doesn't actually delete the files, at least not straight-away. It moves the dim images to a folder named "trash" and then you can take a look before removing them. If you want to use a different name than "trash", then use the argument `--trash`

```
$ npm run clean -- -f ./HDTv/trash-Wars-S21 --trash refuse
```

Note: the "trash" folder will be ignored and will be set to whatever new name this option is given

###### Delete

Remove the "trash" folder when done scanning the images

#### Cleaning (image) Options

##### Tolerance (luminance aka brightness)

The tolerance must be a number between 0 and 1. The decimal is limited to 2 digits (no values between 0.01 and 0.02). This number determines the maximum level of brightness for a pixel. Any values exceeding the number given will be passed over. To delete images with levels below 0.1:

```
$ npm run clean -- -f ./some-folder -t 0.1
```

##### Percentage (luminance dominance)

Like the tolerance option, the percentage looks at the dominant brightness level and compares the percentage. This is how you determine if a certain brightness takes up a large "percentage" of an image. Try to keep this level generally high (0.8 or higher) to avoid deleting too many frames:

```
$ npm run clean -- -f ./some-folder -p 0.8
```

Note: The tolerance for luminance is checked before this is inspected. There will be no levels higher than the tolerance, but within a tolerable percentage that ends up being deleted.

##### Quick (sorting)

To speed up the process, the files are sorted by size in ascending order. Since images are primarily dim have smaller file sizes than bright ones, they can be inspected before larger ones. This reduces the potential hours of processing for thousands of images to a few minutes.
If you want to be more thorough, you can turn off the `--quick` option, give it the value `false`. This will sort the files by name in ascending order, and descending order. This is helful for videos with many dark images and the beginning and end. Once those sections are inspected, then it will sort them by size like the `--quick` option does

Note: there is no way to read every single file. Maybe if you specify a lot of `attempts`

##### Attempts

For each "section" of the images that the script looks at, it will give up after a certain number attempts. After the script sorts the files, it will read from the beginning of the sorted list. If the file it reads is too bright, then the number of `attempts` goes up by one. If it encounters a dark image, then it resets this counter to 0, effectively starting over the inspection of that section. For example, if you wanted to look at least the first 50 files, then use the command:

```
$ npm run clean -- -f ./some-folder --attempts 50
```

#### Debug

Look at the luminance and the progress of the cleaning script helps with fine-tuning the best options for your files. Use the `--debug` option to get the most out of this 😊 Here is a snippet of what it looks like:

```
/mnt/d/gumbyscreens/1956/gumby_business/gumby_business_0105.jpg: 1.860s
{
        0.00=100%,
}
key 0.0 <= tolerance 0.15 ? (true)
deleting /mnt/d/gumbyscreens/1956/gumby_business/gumby_business_0105.jpg
/mnt/d/gumbyscreens/1956/gumby_business/gumby_business_0880.jpg: 2.196s
{
        0.00=76%,       0.45=4%,        0.46=4%,        ...
}
found 6 key(s) > threshold 0.15 [ 0.45, 0.46, 0.47, ... ]
...
```

this shows how long it took to read a file, its luminance, and whether or not it decided to delete the file.

---

# Posting to Twitter

This repo relies on three components:

1. A host for the files
1. A scheduled task
1. A twitter app

The following sections help explain what your options are. I use Digital Ocean for hosting the files, Heroku for scheduling the tweets, and a developer app on Twitter.

Tweeting these frames requires an app with hosting options and a twitter developer app

- A host with API access
  - S3
    - https://www.digitalocean.com/community/tutorials/how-to-create-a-digitalocean-space-and-api-key
    - You can look up how to create a token in AWS right? ;)
  - Dropbox (Deprecated)
    - https://www.dropbox.com/developers/reference/getting-started
- Social media account access
  - Twitter
    - https://developer.twitter.com/en/docs/apps/overview

### Hosting

These are the supported options for hosting files:

- Spaces / S3
  - Digital Ocean offers a S3-like hosting that is reasonably-priced
  - And there's always AWS ...
- Dropbox (deprecated)
  - Seems like the tokens will still work if you have one

I suppose local file disk systems and FTP could be added, but that doesn't seem realistic for your run-of-the-mill "I want to make a funny bot" type of user. Generally, you're going to be hosting so many files and want to have it running forever, so unless it's a quick experiment, running it on your local machine won't be much fun 📂

### Scheduling

I highly recommend Heroku, but if you're using DO or AWS, then it's pretty simple to schedule a CRON job on a droplet or EC2 instance. Using the scheduler add-on for Heroku is reliable, but won't run exactly on-time. At it's worst it's been 4 or 5 minutes late to start the task, but that's fine with me. If you require accurate timing, then I'd use a cron scheduler and `ntp` (https://linux.die.net/man/8/ntpd)

#### Heroku

Free-tier dynos are great. Create an app. Install the Scheduler Add-on. https://elements.heroku.com/addons/scheduler
Update the app's config to connect to the host and twitter. Then schedule a job that runs the script: `npm run tweet`. Couldn't be easier. Now only if they offered cloud storage ☁

#### Digital Ocean and AWS

It may cost money, but DO Spaces is about the same prices as Dropbox and Google Drive. Spaces is also interchangeable with S3, just a few quirks in the settings, but it's virtually identical.

#### Transferring Frames

OK if you have _a lot_ of files to move over then you really can't rely on Dropbox if you care about accuracy. The browser upload works, but can't promise it'll work for thousands... So I suggest `s3cmd` (https://s3tools.org/s3cmd) for putting the files on the host. Once you have it installed and configured then you can run the command like:

```
$ cd Net\ Cafe
$ s3cmd * s3://BUCKET-NAME/FOLDER_NAME/ --recursive
```

#### S3 Authentication

Pretty straight-forward on the API key creation process. Copy and paste it in to the `.env`. I included one as an example:

```
AWS_ACCESS_KEY_ID=...
AWS_SECRET_ACCESS_KEY=...
ENDPOINT=nyc3.digitaloceanspaces.com
REGION=nyc3
BUCKET=...
```

##### Dropbox (deprecated)

This repo was originally writtern with Dropbox as the primary host. Unfortunately they have deprecated long-term access tokens https://dropbox.tech/developers/migrating-app-permissions-and-access-tokens 💀 This means instead of generating a token once for access, you have to request for a "refresh" token each time you want to use it. Since this complicates the process this repo uses, it will no longer be supported (PRs welcome)

If you generated a long-term access token before they were deprecated, then it should still work, we just don't know for how much longer, or how reliable it will be from now on.

I considered using Google Drive as an option, and if there's any outside any interest, I can add support for it. I would imagine more people pay for storage on Drive than Dropbox, and the API access part should be simpler.

##### Dropbox Authentication (deprecated)

This probably doesn't work anymore but, go to the _App Console_ and look for the **Generated access token** button. Copy and paste this token value into `.env`

```
#dropbox settings
ACCESS_TOKEN=...
```

#### Twitter Authentication

After creating a twitter application, you'll have a `CLIENT_ID` and `CLIENT_SECRET` for making requests on your application's behalf. Save these to the `.env`

```
#twitter settings
CLIENT_ID=...
CLIENT_SECRET=...
```

### Logging in to Twitter

You will need another pair of keys to actually tweet as someone else. I've added a `login` script that will host a server on your machine:

```
$ npm run login
waiting to authenticate Twitter login on port localhost:3000/login/twitter
```

Make sure to update your list of callback URL's on your Twitter app. If it's running on port `3000` like in the example, then it should be set to `http://localhost:3000/login/twitter`. Be sure to set the same URL as the `CALLBACK_URL` in the `.env` file too. There is also a `SESSION_SECRET` variable that needs to be set. This is like a password and should be treated as such:

```
#express and passport settings
CALLBACK_URL=http://localhost:3000/login/twitter
SESSION_SECRET=*** <-- Change this value for your app and keep it secret :)
```

#### Getting Access Tokens

Go to `localhost:3000/login/twitter` with your browser and it will redirect you through the Twitter OAuth process. You may need to log out of any sessions in order for this to work... Eventually you'll get a JSON response with the necessary tokens :)

The required environment variables for the `.env` file are:

```
#twitter settings
CONSUMER_KEY=...
CONSUMER_SECRET=...
ACCESS_TOKEN_KEY=...
ACCESS_TOKEN_SECRET=...

#express and passport settings
CALLBACK_URL=http://localhost:3000/login/twitter
SESSION_SECRET=...
```

### Post the Tweet

```
npm run tweet -- -f folder -s S3 -b my-bucket --workspace .
```

This says to tweet from s3, save the file to the current location `.`, and select a frame from the folder `folder` in the bucket, `my-bucket`. The default location for saving the file is the current location so it can be left out. You can also the `.env` file to accomplish the same thing:

```
FOLDER=folder
SOURCE=S3
BUCKET=my-bucket
```

and then run the script with no options

```
npm run tweet
```

- note: you will need the twitter and s3 environment variables along with the ones listed above

#### Heroku to Twitter

Use the heroku-scheduler for triggering the tweet process:
https://devcenter.heroku.com/articles/scheduler

Create a job that uses the command `npm run tweet` and set the interval that works best for your application. Here is a list of the required settings when tweeting from Heroku:

##### Configuing the Heroku App

If you have the heroku-cli installed on your machine, then you can use a script to automatically set all of the environment variables from your local `.env` file to the app on heroku (instead of entering them one at a time on the dashboard). This script is meant for using s3 (or spaces) as the hosting provider. Before running this script, make sure you have the following varialbes set in the `.env`:

```
# twitter settings
CONSUMER_KEY=...
CONSUMER_SECRET=...
ACCESS_TOKEN_KEY=...
ACCESS_TOKEN_SECRET=...

# s3 settings
SOURCE=s3
AWS_ACCESS_KEY_ID=...
AWS_SECRET_ACCESS_KEY=...
FOLDER=...
BUCKET=...
ENDPOINT=...
REGION=...

# heroku settings
HEROKU_APP_NAME=MyHerokuAppName
REDIRECT_URL=https://twitter.com/myfunnybot
```

You can also look at all of these by using the `--help` flag:

```
$ npm run heroku -- --help
```

This doesn't _immediately_ set the variables, but it does create the script needed to do so. Adjust the files permissions so that the script it executable, and then run it.

```
$ npm run heroku
$ chmod u+x heroku.sh
$ ./heroku.sh
Setting: REDIRECT_URL and ...
```

###### Additional Options

There is a flag that is used to remove any existing dropbox configurations while it's updating the settings. This is meant for changing from dropbox to s3:

```
$ npm run heroku -- --unset-dbx
```

This removes the settings `"CLIENT_ID"`, `"CLIENT_SECRET"`, and `"ACCESS_TOKEN"` from the app on heroku.

Also you can change the filename of the script it generates by using the `--script` option:

```
$ npm run heroku -- --script ../MyHerokuConfigScript.sh
```

##### Just a little redirect

One downside to this approach is that you have a dedicated URL with not much to show (it is on the bird app after all). I've created a simple module that will redirect any request set by the `REDIRECT_URL` in the `.env` file.

```
$ npm run start

> frame-bot@2.0.0 start /home/dc/cod/frame-bot
> node src/index.js

waiting to redirect requests on port 3000 to https://twitter.com/gumbyscreens
```

What this all really means is that if someone finds your Heroku URL for this bot like: https://gumby-screens.herokuapp.com then they will be redirected to the bot on twitter. Lazy ain't it? 🍹

---

# Choosing Frames

This feature is still a bit experimental, but you can use a database to keep track of all the frames, and which ones have been tweeted. There are plans to add support for multiple databases, but so far the only option now uses MongoDB. This also only supports S3/Spaces as a host; Dropbox is being slowly phased out as an option

### Configuring for database access

```
; Mongo Settings
DB_NAME=net_cafe
DB_USER=...
DB_PASS=...
DB_HOST=...

; S3 Settings
SOURCE=S3
FOLDER=net_cafe
AWS_ACCESS_KEY_ID=...
AWS_SECRET_ACCESS_KEY=...
ENDPOINT=...
REGION=...
BUCKET=...

```

It is not necessary to have the `DB_NAME` and `FOLDER` match, but it does make it easier to remember which database corresponds to a folder. Free hosting for MongoDB is available at mongodb.com. Once you have a DB up and running, use the connection details to fill out those .env settings.

### Populating the database

First you will need to insert records into the "frames" collection. After configuring your settings like above, run the following command:

```
$ npm run populate-mongo
```

This will create (or drop if it already exists) a database named as `DB_NAME` and create a "frames" collection inside of it. Each document inside of it will have the name of each frame from `FOLDER` with some metadata.

## Frame choice options

The process for choosing a "random" frame is lazy, but fast. It essentially keeps rolling the dice, checking against the ones already selected, and repeats until it finds a unique frame. These choices rely on two deciding factors:

1. Has this frame been tweeted before?
1. Has a frame from a similar location been tweeted recently?

The first factor is straight-forward; If a frame has already been chosen before, then we make another random choice until we find one that doesn't match one of the ones used earlier.

The second factor is more nuanced and relies on three settings:

1. _Threshold_: The ratio of `(tweets / frames)` represents how many of the frames have been tweeted (e.g. 100/200 = 50% frames have been tweeted). When the specified threshold is met, the tweets collection is dropped. Recent tweets are inserted back into it and that depends on the next setting, _memory_.
1. _Memory_: This looks up the most recent tweets and which frames it had used. The number given to _memory_ specifies how far back it goes. This collection of frames is used for two purposes:
   1. It inserts the tweets back into the collection after the threshold has been met and the collection is dropped.
   1. It compares names with recent tweets against the random choices to avoid "near-duplicates" (used with _depth_).
1. _Depth_: Used with _memory_. When choosing which frame to use, the names of the frames are split up by the '/' character. The depth option selects which index to choose from the array:
   - name = "net_cafe/web_comics104/web_comics_0012.png"
   - array = [net_cafe, web_comics104, web_comics_0012.png])
   - depth = 1;
   - selected = array[depth] = "web_comics104"

### Stress testing database choices

Before we jump into how to tweet with the database connection, it's good to fine-tune some of the settings for choosing which frame to tweet. The `--stress` option is used to simulate how the choice process works for the selected database. Note: this will _not_ post any tweets. They will be inserted into the "tweets" collection, but nothing will be posted online.

```
$ npm run tweet-mongo -- --stress
```

You can modify the `--threshold`, `--memory`, and `--depth` when running the stress test to see how it performs. Also include the `--debug` option to see more details during it's selection process.

The default settings should handle _most_ cases, but it depends on how your frames are organized. For example, if I wanted to increase the memory to 100 and use the 2nd index for name comparison:

```
$ npm run tweet-mongo -- --stress --memory 100 --depth 2
```

Use a variety of combinations and be mindful of how many unique choices exist within your database. For instance, if there are 10 folders containing your frames, use a number less than 10 for your `--memory` option, otherwise it will get stuck in a loop looking for a unique entry.

#### Saving settings

To avoid having to specify these arguments every time, you can save them to your .env like so:

```
FRAME_THRESHOLD=0.7
FRAME_MEMORY=10
FRAME_DEPTH=2
```

#### Tweeting from mongo

Now that you've found the optimal settings for your project and saved them, you can now schedule a task to run the tweet command:

```
$ npm run tweet-mongo
```

---

# testing

The tests are kind of a mess, and I should be using VCR for storing the Twitter and Dropbox requests. So be warned that running the tests with all of the `.env` settings would send an actual tweet (woops!)

```
npm run test
```

### testing-assets

There is a `test/assets` folder that contains a test (music) video for analysis. Sorry about the size (6MB), here's the source if you'd rather preview it https://www.youtube.com/watch?v=xi5LZIw_LQU

These tests will be expecting this video to be available in the `test/assests` folder, and will be rendering images to the `test/assets/dump` folder. Image rendering is based on what is set in the `.env` file.

There are `pending` tests, until you set the `FOLDER=...` setting your `.env` file. This is represents your remote Dropbox folder. Features for this service and Twitter will be pending until these settings are defined.

---

# history

The first "frame-bot" project can be found here https://github.com/dudethatbe/frame-bot. I mostly wanted to start-over on this repo and use GitLab instead of GitHub. I plan on making as many breaking changes as I can, at least until someone else wants to contribute.
