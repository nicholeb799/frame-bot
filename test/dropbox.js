const { dropboxFolderExists } = require("./assets/paths.json");
const { analyzeDropboxUploads } = require("../src/bin/analyze_dropbox_uploads");
const { describe } = require("mocha");
const { equal } = require("chai").assert;
describe("Dropbox Analysis", function () {
  describe("#analyzeDropboxUploads", function () {
    it("should be able to count the files and folders from a dropbox folder", async function () {
      if (!process.env.ACCESS_TOKEN) {
        this.skip();
      } else {
        const results = await analyzeDropboxUploads(dropboxFolderExists);
        return equal(
          results.folders.length,
          1,
          "should be able to read a dropbox folder"
        );
      }
    });
  });
});
