const { unlink } = require("fs").promises;
const { postTweet } = require("../lib/twitter");

async function chooseFileFromDropbox(folder, saveTo) {
  const { chooseRandomFrame, downloadFile } = require("../lib/dropbox");
  console.log(`Choosing a file to download from ${folder} ...`);
  const randomFrame = await chooseRandomFrame(folder);
  const path = randomFrame.path_lower;
  const { file, response } = await downloadFile(path, saveTo);
  console.log(`Downloaded ${file}`);
  return file;
}
async function chooseFileFromSpaces(folder, bucket, saveTo) {
  const { chooseRandomObject, downloadObject } = require("../lib/s3");
  console.log(`Choosing a file to download from [${bucket}]/${folder} ...`);
  const object = await chooseRandomObject(bucket, folder);
  const file = await downloadObject(bucket, object["Key"], saveTo);
  console.log(`Downloaded ${object["Key"]}`); // print out the filename aka the "Key"
  return file;
}
exports.tweetFile = async (file) => {
  const tweet = await postTweet(file);
  console.log(`${tweet.created_at} => ${tweet.text}`);
  return tweet;
};
exports.tweetFromDropbox = async (folder, saveToFolder) => {
  const file = await chooseFileFromDropbox(folder, saveToFolder);
  await this.tweetFile(file);
  await unlink(file);
};
exports.tweetFromS3 = async (folder, bucket, saveToFolder) => {
  const file = await chooseFileFromSpaces(folder, bucket, saveToFolder);
  await this.tweetFile(file);
  await unlink(file);
};
