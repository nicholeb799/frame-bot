const { getImagesFromPath } = require("../lib/videoFinder");
const { analyzeFrames } = require("../lib/frameMaker");

const analyzeFramesBin = async function (readFrom, writeTo) {
  if (!readFrom) {
    console.error("Unable to detect a folder to read from!");
    return;
  }
  if (!writeTo) {
    writeTo = "./results.csv";
  }
  const images = await getImagesFromPath(readFrom);
  return analyzeFrames(images, writeTo);
};
module.exports.analyzeFrames = analyzeFramesBin;
