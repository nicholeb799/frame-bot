require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const { makeFrames } = require("./bin/make_frames");
const options = [
  groups.pathOptions,
  args.folder,
  args.output,
  args.format,
  groups.frameOptions,
  args.interval,
  args.subtitles,
  args.trim,
  args.start,
  args.end,
  groups.croppingOptions,
  args.x,
  args.y,
  args.width,
  args.height,
  groups.scalingOptions,
  args.scale,
  groups.emptyOptions,
  args.debug,
  args.help,
];

(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("make.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log("usage: npm run make-frames [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (!opts.folder) {
    console.error("Folder is missing '-i INPUT'");
    return;
  }

  if (!opts.output) {
    console.error("Output folder is missing '-o OUTPUT'");
    return;
  }

  if (!opts.interval) {
    console.error("Unable to detect interval '-n INTERVAL'");
    return;
  }

  if (opts.interval <= 0) {
    console.error("Interval must be greater than 0");
    return;
  }
  if (opts.scale && !(opts.width || opts.height)) {
    console.error(
      "Scaling option given, but neither a --width or -height given"
    );
    return;
  }

  if (opts.trim) {
    if (!(opts.start || opts.end)) {
      console.error(
        "Trimming option given, but there's no usage of --start or --end"
      );
      return;
    }
    if (opts.start < 0 || opts.end < 0) {
      console.error("Start or End trim cannot be less than 0");
      return;
    }
  }

  makeFrames(
    opts.folder,
    opts.output,
    opts.format,
    opts.scale,
    opts.width,
    opts.height,
    opts.interval,
    opts.subtitles,
    opts.trim,
    opts.start,
    opts.end,
    opts.x,
    opts.y,
    opts.debug
  );
})();
