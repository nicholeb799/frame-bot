require("dotenv").config();
const dashdash = require("dashdash");
const { makeHerokuConfigScript } = require("./bin/heroku");
const { groups, args } = require("./const/options.json");
const options = [
  groups.cloudOptions,
  args.source,
  args.folder,
  args.awsAccessKeyId,
  args.awsSecretAccessKey,
  args.bucket,
  args.endpoint,
  args.region,
  groups.twitterOptions,
  args.consumerKey,
  args.consumerSecret,
  args.accessToken,
  args.accessTokenSecret,
  groups.herokuOptions,
  args.herokuAppName,
  args.redirectUrl,
  groups.scriptOptions,
  args.script,
  args.unsetDbx,
  groups.emptyOptions,
  args.help,
];
(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("heroku.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log("usage: npm run heroku -- [OPTIONS]\n" + "options:\n" + help);
    return;
  }
  await makeHerokuConfigScript(opts);
})();
