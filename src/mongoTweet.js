require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const options = [
  groups.pathOptions,
  args.folder,
  groups.cloudOptions,
  args.awsAccessKeyId,
  args.awsSecretAccessKey,
  args.endpoint,
  args.region,
  args.bucket,
  groups.dbOptions,
  args.host,
  args.name,
  args.user,
  args.pass,
  args.stress,
  groups.choiceOptions,
  args.threshold,
  args.memory,
  args.depth,
  args.trimName,
  groups.emptyOptions,
  args.debug,
  args.help,
];
const {
  chooseFrame,
  getFrameAndTweetCounts,
  getClient,
} = require("./bin/tweet_mongo");
const { tweetFile } = require("./bin/tweet");
const { downloadObject } = require("./lib/s3");
(async () => {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("mongo.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log(
      "usage: npm run populate-mongo --  [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }

  if (!opts.name) {
    console.error(
      "unagle to detect DB_NAME in .env or argument '--name DB_NAME'"
    );
    return;
  }
  if (!opts.folder) {
    console.error("unable to detect FOLDER in .env or argument '-f FOLDER'");
    return;
  }

  if (!opts.bucket) {
    console.error("Unable to detect BUCKET in .env or argument '-b BUCKET'");
    return;
  } else {
    if (opts.stress) {
      console.time("stress");
      const { framesCount, tweetsCount } = await getFrameAndTweetCounts(
        opts.name
      );
      console.log(`${tweetsCount} tweets made; ${framesCount} frames in db`);
      const client = await getClient();
      // tweets however times it takes to go one over the edge
      const totalTweets = framesCount - tweetsCount + 1;

      for (let i = 0; i <= totalTweets; i++) {
        // console.time("chooseFrame");
        const frame = await chooseFrame(
          opts.name,
          opts.threshold,
          opts.memory,
          opts.depth,
          opts.trimName,
          opts.debug,
          client
        );
        const percentage = parseFloat(((i / totalTweets) * 100).toFixed(2))
          .toFixed()
          .padStart(3);
        console.log(`${percentage}%`, frame.name);
        aw;
      }
      await client.close();
      console.timeEnd("stress");
    } else {
      console.log(`choosing a frame from ${opts.folder} ...`);
      const frame = await chooseFrame(
        opts.name,
        opts.threshold,
        opts.memory,
        opts.depth,
        opts.trimName,
        opts.debug
      );
      const frameKey = frame.name;
      console.log(frameKey);
      const frameObject = await downloadObject(opts.bucket, frameKey, ".");
      await tweetFile(frameObject);
    }
  }
})();
