require("dotenv").config();
const dashdash = require("dashdash");
const { cleanFrames } = require("./bin/clean");
const { groups, args } = require("./const/options.json");
const options = [
  groups.pathOptions,
  args.folder,
  args.ignorePath,
  args.trashPath,
  args.deleteTrash,
  groups.cleanOptions,
  args.luminanceTolerance,
  args.luminancePercentage,
  args.quickSort,
  args.attempts,
  groups.emptyOptions,
  args.debug,
  args.help,
];
(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("clean.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log("usage: npm run clean -- [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (opts.ignore !== opts.trash) {
    opts.ignore = opts.trash;
  }

  cleanFrames(
    opts.folder,
    Number(opts.percentage),
    Number(opts.tolerance),
    opts.delete,
    opts.ignore,
    opts.trash,
    opts.attempts,
    opts.quick,
    opts.debug
  );
})();
