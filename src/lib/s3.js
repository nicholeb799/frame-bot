const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const spacesEndPoint = new AWS.Endpoint(process.env.ENDPOINT);
const spacesRegion = process.env.REGION;
const s3 = new AWS.S3({
  endpoint: spacesEndPoint,
  region: spacesRegion,
});
async function listObjects(bucket, folder, token) {
  var params = {
    Bucket: bucket,
    Prefix: folder,
  };
  if (token) {
    params["ContinuationToken"] = token;
  }

  try {
    return s3.listObjectsV2(params).promise();
  } catch (e) {
    console.error(e);
  }
}
exports.getObjects = async (bucket, folder) => {
  let objects = [];
  let response = await listObjects(bucket, folder);
  let contents = response.Contents;
  objects = objects.concat(contents);
  while (response.IsTruncated) {
    let token = response.NextContinuationToken;
    response = await listObjects(bucket, folder, token);
    objects = objects.concat(response.Contents);
  }
  return objects;
};
exports.chooseRandomObject = async (bucket, folder) => {
  const objects = await this.getObjects(bucket, folder);
  const rand = Math.floor(Math.random() * objects.length);
  return objects[rand];
};
exports.downloadObject = async (bucket, key, dest) => {
  const file = path.resolve(dest, path.basename(key));
  const write = fs.createWriteStream(file);
  return new Promise((resolve, reject) => {
    s3.getObject({ Bucket: bucket, Key: key })
      .createReadStream()
      .on("end", () => {
        resolve(path.resolve(file));
      })
      .on("error", (e) => {
        reject(e);
      })
      .pipe(write);
  });
};
