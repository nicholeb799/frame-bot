const { describe } = require("mocha");
const { equal, isAbove } = require("chai").assert;
const {
  countUploads,
  chooseRandomFrame,
  downloadFile
} = require("../../src/lib/dropbox");
const {
  testFolderForDownloads,
  dropboxFolderNotExists,
  dropboxFolderExists
} = require("../assets/paths.json");
describe("Dropbox", function () {
  describe("#chooseRandomFrame()", function () {
    it("should be able to find a 'frame' from a random folder", async function () {
      if (!process.env.ACCESS_TOKEN) {
        this.skip();
      } else {
        this.timeout(5000);
        const randomFrame = await chooseRandomFrame(dropboxFolderExists);
        return Promise.all([
          isAbove(
            randomFrame.size,
            50,
            "expected random frame size to be greater than 50 bytes"
          ),
          equal(randomFrame[".tag"], "file", "expected frame tag to be file")
        ]);
      }
    });
  });
  describe("#downloadFile()", function () {
    it("should be able to download a 'frame' from a random folder", async function () {
      if (!process.env.ACCESS_TOKEN) {
        this.skip();
      } else {
        this.timeout(5000);
        const randomFrame = await chooseRandomFrame(dropboxFolderExists);
        const { file, response } = await downloadFile(
          randomFrame.path_lower,
          testFolderForDownloads
        );
        console.log(file, response.result.size);
        return Promise.all([
          isAbove(
            file.split("/").length,
            1,
            "expected at least one folder in the destination path"
          ),
          isAbove(
            response.result.size,
            50,
            "excpected the response object to be greater than 50 bytes"
          )
        ]);
      }
    });
  });
  describe("#countUploads()", function () {
    it("should be able to count the number of uploads from a folder on dropbox", async function () {
      if (!process.env.FOLDER || !process.env.ACCESS_TOKEN) {
        this.skip();
      } else {
        this.timeout(5000);
        const response = await countUploads(dropboxFolderExists);
        return Promise.all([
          isAbove(
            response.size,
            1000,
            "expected the response size to be greater than 1000 bytes"
          ),
          isAbove(
            response.folders.length,
            0,
            "expected at least one folder in the response"
          ),
          isAbove(
            response.files.length,
            0,
            "expected at least one file in the response"
          )
        ]);
      }
    });
    it("should be able to fail gracefully when a non-existant folder is referenced", async function () {
      if (!process.env.FOLDER || !process.env.ACCESS_TOKEN) {
        this.skip();
      } else {
        const response = await countUploads(dropboxFolderNotExists);
        return Promise.all([
          equal(response.size, 0, "expected response size to equal 0"),
          equal(
            response.folders.length,
            0,
            "expected no folders to be in the response"
          ),
          equal(
            response.files.length,
            0,
            "expected no files to be in the response"
          )
        ]);
      }
    });
  });
});
