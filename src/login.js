require("dotenv").config();
const { Strategy } = require("passport-twitter");
const passport = require("passport");
const express = require("express");
const session = require("express-session");
const app = express();
const port = process.env.PORT;
const { CONSUMER_KEY, CONSUMER_SECRET, CALLBACK_URL } = process.env;
if (CONSUMER_KEY && CONSUMER_SECRET && CALLBACK_URL) {
  passport.use(
    new Strategy(
      {
        consumerKey: CONSUMER_KEY,
        consumerSecret: CONSUMER_SECRET,
        callbackURL: CALLBACK_URL
      },
      (token, tokenSecret, profile, cb) => {
        const response = {
          ACCESS_TOKEN_KEY: token,
          ACCESS_TOKEN_SECRET: tokenSecret,
          username: profile.username,
          photos: profile.photos
        };
        return cb(null, response);
      }
    )
  );
} else {
  throw new Error('Unable to detect consumer key, secret, and callback_url')
}
passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.get("/", (req, res) => {
  res.send({
    message: `go to "http://localhost:${port}/login/twitter" to open the OAuth login page`
  });
});
app.get("/login/twitter", passport.authenticate("twitter"));
app.get(
  "/login/twitter/return",
  passport.authenticate("twitter", { failureRedirect: "/login" }),
  function (req, res) {
    res.send({ user: req.user });
  }
);
app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});
app.listen(port, () => {
  console.log(
    `waiting to authenticate Twitter login on port localhost:${port}/login/twitter`
  );
});
module.exports = app;
