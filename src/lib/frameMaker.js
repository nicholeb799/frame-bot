const {
  createFolder,
  probeVideo,
  getVideoStream,
  findSubtitleFile,
} = require("./videoFinder");
const { getFrameLuminance } = require("./clean");
const { promisify } = require("util");
const { readdir, writeFile } = require("fs").promises;
const { parse, resolve } = require("path");
const Papa = require("papaparse");
const exec = promisify(require("child_process").exec);
const maxBuffer = 1024 * 1024 * 5;

exports.analyzeFrames = async (files, writeTo) => {
  const results = [];
  const label = `Time to read ${files.length} files`;
  console.time(label);
  for (const file of files) {
    console.time(file);
    let luminance = await getFrameLuminance(file);
    console.timeEnd(file);
    let luminanceMap = "";
    let i = 0;
    const keys = Object.keys(luminance);
    while (i < 5) {
      if (i < keys.length) {
        const decimal = luminance[keys[i]];
        const percentage = `${parseFloat((decimal * 100).toFixed(2))
          .toFixed(0)
          .padStart(2, "0")}%`;
        luminanceMap += `${keys[i]}=${percentage};`;
      }
      i++;
    }
    let result = { file, luminance: luminanceMap };
    results.push(result);
  }
  console.timeEnd(label);
  const csvData = Papa.unparse(results);
  await writeFile(writeTo, csvData);
  return results;
};
exports.analyzeVideos = async (files, writeTo) => {
  const results = [];
  for (const file of files) {
    const details = await probeVideo(file);
    if (!details.streams) {
      console.warn(`Unable to detect any streams for ${currentFile}`);
      continue;
    }
    try {
      const videoStream = getVideoStream(details);
      const { width, height, duration, size } = videoStream;
      results.push({ file, width, height, duration, size });
    } catch (e) {
      console.warn(`unable to parse ${file}`);
      results.push({ file, width: 0, height: 0, duration: 0, size: 0 });
    }
  }
  const csvData = Papa.unparse(results);
  await writeFile(writeTo, csvData);
  return results;
};

exports.makeFrames = async (
  inputFolder,
  files,
  saveTo,
  fileFormat,
  scaleFrames,
  frameWidth,
  frameHeight,
  frameInterval,
  includeSubtitles,
  trim,
  trimStart,
  trimEnd,
  cropX,
  cropY,
  debug
) => {
  // trim last slash off of input folder; used to find subFolders
  inputFolder = inputFolder.replace(/\/$/, "");
  const parsedOutput = parse(saveTo);

  const parenthesesRegex = /(\(|\)| |')/g;
  let numberOfFrames = 0;

  for (const file of files) {
    const parsedFile = parse(file);
    let pathPieces = [parsedOutput.dir, `${parsedOutput.base}/`];
    let subFolders = parsedFile.dir.replace(inputFolder, "");
    if (subFolders) {
      subFolders = subFolders.replace(/^\//, "");
      pathPieces.push(`${subFolders}`);
    }
    pathPieces.push(`${parsedFile.name}/`);
    const combined = resolve(...pathPieces);
    const currentFile = resolve(parsedFile.dir, parsedFile.base);
    let details;
    try {
      details = await probeVideo(currentFile);
    } catch (error) {
      if (debug) console.log(`Unable to probe ${parsedFile.base}`);
      throw error;
    }

    if (!details.streams) {
      console.warn(`Unable to detect any streams for ${currentFile}`);
      continue;
    }

    const escapedName = currentFile.replace(parenthesesRegex, "\\$1");
    var videoStream;
    try {
      videoStream = getVideoStream(details);
    } catch (e) {
      if (debug)
        console.warn(
          `Unable to get video stream details for ${parsedFile.base}`
        );
      continue;
    }
    let { duration, width, height } = videoStream;

    if (trim && trimStart + trimEnd + frameInterval >= duration) {
      console.warn(
        `Combined starting trim, ending trim, and interval are greater than or equal to the duration, skipping ${parsedFile.base}`
      );
      console.warn(
        `(${trimStart} + ${trimEnd} + ${frameInterval}) >= ${duration}`
      );
      continue;
    }

    const createdFolder = await createFolder(combined);
    if (!createdFolder) {
      throw new Error(`Unable to create folder "${combined}"`);
    } else {
      const frameName = `${combined}/${parsedFile.name}_%04d.${fileFormat}`;
      const escapedOut = frameName.replace(parenthesesRegex, "\\$1");
      var cmd = "ffmpeg -hide_banner";
      if (trimStart) {
        cmd += ` -ss ${trimStart}`;
      }
      cmd += ` -i ${escapedName} -vf `;

      if (cropX) {
        // crop=width:height:x:y
        // choose frameWidth if defined, else default video width
        let cropExpression = frameWidth ? `${frameWidth}:` : `${width}:`;
        // same for height
        cropExpression += frameHeight ? `${frameHeight}:` : `${height}:`;
        // cropX is a constant
        cropExpression += `${cropX}:`;
        // choose cropY if defined, else frameHeight, else default video height
        cropExpression += cropY
          ? `${cropY}`
          : frameHeight
          ? `${frameHeight}`
          : `${height}`;
        if (cropExpression.split(":").length === 4) {
          cmd += `crop=${cropExpression},`;
        } else {
          if (debug) {
            console.warn(
              `Unable to build valid crop expression (${cropExpression})`
            );
          }
        }
      }

      if (scaleFrames && (frameWidth || frameHeight)) {
        let scaleWidth = frameWidth ? frameWidth : width;
        let scaleHeight = frameHeight ? frameHeight : height;
        cmd += `scale=${scaleWidth}:${scaleHeight},`;
      }

      if (includeSubtitles) {
        const subtitleFile = await findSubtitleFile(
          parsedFile.dir,
          currentFile,
          parsedFile.ext
        );
        if (subtitleFile) {
          cmd += `subtitles=${subtitleFile}:force_style=\'BackColour=&H60000000\\,BorderStyle=4\\,Outline=0\\,Fontsize=22\',`;
        } else {
          if (debug) {
            console.log("unable to find subtitle for current file");
            console.log(currentFile);
          }
        }
      }

      let startAt = 0;
      if (trim) {
        if (trimStart > 0) {
          startAt = trimStart;
        }
        if (trimEnd) {
          duration -= trimEnd;
        }
      }

      // select frames
      cmd +=
        `select=\'between(t\\,${startAt}\\,${duration})*` +
        `not(mod(n\\,${frameInterval}))'`;

      // write out
      cmd += ` -vsync vfr -q:v 2 ${escapedOut}`;

      if (debug) {
        console.log(cmd);
      } else {
        console.log(`saving frames from ${parsedFile.name} to ${combined} ...`);
      }

      const { stderr, stdout } = await exec(cmd, {
        maxBuffer: maxBuffer,
      });
      if (stderr) {
        // console.log(stderr);
      }
      if (stdout) {
        // console.log(stdout);
      }
      const dirents = await readdir(combined);
      console.log(`saved ${dirents.length} frames to ${combined}`);
      numberOfFrames += dirents.length;
    }
  }
  return numberOfFrames;
};
