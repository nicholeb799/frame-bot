function toTwoDigitNum(value) {
  return value.toFixed(2) * 100;
}
exports.floatToPercentStr = (value) => {
  return toTwoDigitNum(value).toFixed(0) + "%";
};
exports.floatToNum = (value) => {
  return toTwoDigitNum(value);
};
