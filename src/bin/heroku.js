const { promisify } = require("util");
const { writeFile } = require("fs");
const promiseWrite = promisify(writeFile);
exports.makeHerokuConfigScript = async function (opts) {
  const {
    aws_access_key,
    aws_secret_key,
    bucket,
    folder,
    source,
    endpoint,
    region,
    consumer_key,
    consumer_secret,
    access_token_key,
    access_token_secret,
    redirect_url,
    heroku_app_name,
    unset_dbx,
    script,
  } = opts;
  var settings = [];
  // file-host settings
  if (source) settings.push(`SOURCE=${source}`);
  if (aws_access_key) settings.push(`AWS_ACCESS_KEY_ID=${aws_access_key}`);
  if (aws_secret_key) settings.push(`AWS_SECRET_ACCESS_KEY=${aws_secret_key}`);
  if (bucket) settings.push(`BUCKET=${bucket}`);
  if (folder) settings.push(`FOLDER=${folder}`);
  if (endpoint) settings.push(`ENDPOINT=${endpoint}`);
  if (region) settings.push(`REGION=${region}`);

  // twitter settings
  if (consumer_key) settings.push(`CONSUMER_KEY=${consumer_key}`);
  if (consumer_secret) settings.push(`CONSUMER_SECRET=${consumer_secret}`);
  if (access_token_key) settings.push(`ACCESS_TOKEN_KEY=${access_token_key}`);
  if (access_token_secret)
    settings.push(`ACCESS_TOKEN_SECRET=${access_token_secret}`);

  // heroku settings
  if (redirect_url) settings.push(`REDIRECT_URL=${redirect_url}`);

  const expected = 12;
  if (settings.length < expected) {
    console.error(
      `expected ${expected} properties but only found ${settings.length}`
    );
    return;
  }
  console.log(`creating script to update heroku app ...`);
  var scriptContents =
    "#/bin/bash\n" +
    settings
      .map((f) => `heroku config:set -a ${heroku_app_name} ${f}`)
      .join("\n");

  if (unset_dbx) {
    const removeContents = ["CLIENT_ID", "CLIENT_SECRET", "ACCESS_TOKEN"]
      .map((f) => `heroku config:unset -a ${heroku_app_name} ${f}`)
      .join("\n");
    scriptContents += "\n" + removeContents;
  }
  await promiseWrite(script, scriptContents);
  console.log(
    `update script located at ${opts.script}, file may need to be set to executable before running (chmod u+x)`
  );
};
