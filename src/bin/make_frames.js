const {
  getVideoFilesFromPath,
  entryExists: folderExists,
  createFolder,
  entryExists,
} = require("../lib/videoFinder");
const { makeFrames } = require("../lib/frameMaker");
module.exports.makeFrames = async function (
  folder,
  output,
  format,
  scale,
  width,
  height,
  interval,
  subtitles,
  trim,
  start,
  end,
  x,
  y,
  debug
) {
  const dumpExists = await entryExists(output);
  if (!dumpExists) {
    if (debug) {
      console.log(`Creating output folder "${output}"`);
    }
    await createFolder(output);
  }
  let files = [];
  try {
    files = await getVideoFilesFromPath(folder);
  } catch (error) {
    console.error("there was an error:", error.message);
    throw error;
  }
  if (files.length < 1) {
    console.error(`Sorry, unable to detect any video files from ${input}`);
  } else {
    // read video files
    const label = `Time to process ${files.length} files`;
    console.time(label);
    const numberOfFrames = await makeFrames(
      folder,
      files,
      output,
      format,
      scale,
      width,
      height,
      interval,
      subtitles,
      trim,
      start,
      end,
      x,
      y,
      debug
    );
    console.log(
      `Created ${numberOfFrames} images from ${files.length} files (saved to: ${output})`
    );
    console.timeEnd(label);
  }
};
