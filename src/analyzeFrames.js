require("dotenv").config();
const dashdash = require("dashdash");
const { groups, args } = require("./const/options.json");
const { analyzeFrames } = require("./bin/analyze_frames");
const options = [
  groups.pathOptions,
  args.folder,
  args.results,
  groups.emptyOptions,
  args.help,
];
(async function () {
  const parser = dashdash.createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("analyzeFrames.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run analyze-frames -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }
  analyzeFrames(opts.folder, opts.results);
})();
